#### 1. What are the steps/strategies to do Active Listening?

- Listen to what the other person is speaking and try to comprehend the meaning.
- Avoid thoughts and distractions when someone is speaking.
- Make gestures to show you are listening.
- Don't interrupt the speaker in the middle of a conversation.
- Paraphrase what you have listened to.
- Use phrases that enthusiast the speaker. 
- Taking notes during important conversations.

#### 2. According to Fisher's model, what are the key points of Reflective Listening?

- Eliminate distractions and focus on the conversation.
- Understand the mood of the speaker not only through words but gestures as well.
- Be in the speaker's perspective and try to understand the speaker's situation, thereby allowing them to speak freely.
- Summarize the conversation by using the speaker's own words, rather than some random phrases.
- Don't leave the main subject and try to stick to the point speaker is talking about.
- Take the conversation peacefully and to the point rather than a general chat.

#### 3. What are the obstacles in your listening process?

- When I'm in a noisy environment like people chattering.
- Not feeling good because of some personal issues or family problems.
- When the conversation is boring.

#### 4. What can you do to improve your listening?

- Focus on the conversation and exclude external noises.
- Leave other problems and try to be in the current job.
- Try to make the conversation effective by rephrasing the summary and reflecting on the speaker.

#### 5. When do you switch to Passive communication style in your day to day life?

- If the speaker is one who I like or is important to me.
- When I don't want to be involved in the conversation.


#### 6. When do you switch into Aggressive communication styles in your day to day life?

- The conversation turns into personal life and I don't like it.
- Not feeling good and the person keeps on disturbing me.
- Others are aggressive toward me.
  
#### 7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

- Generally, I avoid Passive Aggressive communication, because talking behind someone is not appreciated and is not good behavior.



#### 8. How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?

- Generically convey our feelings rather than throw our emotions onto another person.
- Beware of our body language and our gestures.
- Keep control of your voice, don't get too smooth or harsh.
- Let the speaker know that they are being aggressive or mean to you and stand up for yourself.
- Asking for what is needed is more important than what we feel.
- Start practicing with closer and known people or family members.
