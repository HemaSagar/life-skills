 # Object Oriented Programming

 OOPs is a programming model or paradigm which is based on the concept of objects that is adapted by many programming languages like Java, C++, Python, C# and so on.

 In pure OOP language, everything inside a program is treated as an object. Even primitive datatypes like `int`, `float`, `char`, and `bool` are also objects. Well known language which is pure OOP is 'Smalltalk'.
 Whereas there are languages that support OOP but are not pure OOP, they are mentioned above.


 ### Why OOPs

 The central feature which makes OOPs stand out is that it binds the data and its processing(methods) together. It allows to break the program into bit sized problems, which is very much useful when the codebase becomes huge.

 

 ### Features of OOPs
 > -  Class
 > -  Objects
 > -  Encapsulation
 > -  Data Abstraction
 > -  Inheritance
 > -  Polymorphism


 ### 1. Classes
   
 A Class is a blueprint or a structure or template for an Object, representing how a particular Object will behave or what an Object can do. It is a user-defined data type that has its own members called data members (instance variables) and methods (functions). These methods work on the data of that particular class. A Class doesn’t get allocated any space in memory when it is created but gets allocated when it is instantiated. 


 **Syntax**

 ``` java
 class <class_name> {  
    data field;  
    method();  
 }
 ```

 



 ### 2. Object
 An Object is an instance of a Class. Objects are created from the templates called Classes. The memory is allocated to an Object when its created, during the runtime or compile time.

 Each programming language has its own way to create an object. In Java, a keyword called `new` is used to create an object.
 
 **Syntax**
 
 ``` Java
 ClassName object = new ClassName();
 ```

 Here goes an example of an Object

 **Example**

 ``` Java
 class Student{
    int age;
    String name;
 
    // this would be invoked when an object is created
    Student(String name, int age) { 
        this.name = name;
        this.age = age;
        
        System.out.println("Constructor called"); 
    }
}
 
class School {
    public static void main(String[] args)
    {
        // this would invoke the  constructor defined in class.
        Geek student1 = new Student("Einstein", 19);
 
        System.out.println(student1.name); // Einstein
        System.out.println(student2.age); // 19
    }
}
 ```

 ### 3. Encapsulation

 The data and methods being wrapped up together is known as **Encapsulation**. It's a way to protect the data and the methods from the outside of the class and make them not accessible to the outer world.
 
 **Data Hiding**  Encapsulation allows us to hide our data variable to the outer classes or users

 The data and the methods are wrapped together which makes to see the code easier.


### 4. Data Abstraction

It's a property through which only essential details are displayed to the user by ignoring and hiding unnecessary details.

There would be a situation where we need to hide the code of our method but only want to show the structure of the method. In such cases, data abstraction comes into the picture, allowing us to hide the code of our method.

In Java, abstraction is achieved by Interfaces and abstract Classes. A Class is called an Abstract Class when the Class contains at least one abstract method.

A method defined abstract must always be redefined in sub class or child class.

**Example**

 ``` Java

 abstract class Shape {
    String color;
 
    // these are abstract methods
    abstract double area();
    public abstract String toString();
 
    // abstract class can have the constructor
    public Shape(String color)
    {
        System.out.println("Shape constructor called");
        this.color = color;
    }
 
    // this is a concrete method
    public String getColor() { return color; }
}
class Circle extends Shape {
    double radius;
 
    public Circle(String color, double radius)
    {
 
        // calling Shape constructor
        super(color);
        System.out.println("Circle constructor called");
        this.radius = radius;
    }
 
    @Override double area()
    {
        return Math.PI * Math.pow(radius, 2);
    }
 
    @Override public String toString()
    {
        return "Circle color is " + super.getColor()
            + "and area is : " + area();
    }}

    public class Test {
    public static void main(String[] args)
    {
        Shape s1 = new Circle("Red", 2.2);
 
        System.out.println(s1.toString());
    }
}
 ```

 In the above case the method area in the Shape class is defined as abstract and then it is redefined in its child class Circle using the `@Override`.

 ### 5. Inheritance

 It's a mechanism through which the other class can inherit the properties of another class.
 **Super Class** - The class whose methods are inherited is known as Super Class.

 **Sub Class** - The class which inherits properties from another class is known as Sub Class.

 **Syntax**

 ```Java
 class derived-class extends base-class  
{  
   //methods and fields  
}
 ```

 The class is inherited using `extend` keyword.

 **Example**

 ```Java
 // Base Class
 class Bicycle {
    // the Bicycle class has two fields
    public int gear;
    public int speed;
 
    // the Bicycle class has one constructor
    public Bicycle(int gear, int speed)
    {
        this.gear = gear;
        this.speed = speed;
    }
 
    // the Bicycle class has three methods
    public void applyBrake(int decrement)
    {
         speed -= decrement;
    }

    // toString() method to print info of Bicycle
    public String toString()
    {
        return ("No of gears are " + gear + "\n"
                + "speed of bicycle is " + speed);
    }   


    //Sub Class

    class MountainBike extends Bicycle {
 
    // the MountainBike subclass adds one more field
    public int seatHeight;
 
    // the MountainBike subclass has one constructor
    public MountainBike(int gear, int speed,
                        int startHeight)
    {
        // invoking base-class(Bicycle) constructor
        super(gear, speed);
        seatHeight = startHeight;
    }
 
    // the MountainBike subclass adds one more method
    public void setHeight(int newValue)
    {
        seatHeight = newValue;
    }
 
    // overriding toString() method
    // of Bicycle to print more info
    @Override public String toString()
    {
        return (super.toString() + "\nseat height is "
                + seatHeight);
    }
}
    
 ```

 There are 3 major types of Inheritance
 - Single Inheritance
 - Multilevel Inheritence
 - Hierarchical Inheritance


### 6. Polymorphism

 Polymorphism in general means one in many forms.
In the same way in OOPs the same method can have different bodies and different parameters with the same name.

 There are two kinds of Polymorphism:
 - Compile time Polymorphism.
 - Runtime Polymorphism.

In compile time polymorphism the functions are overloaded and the operator overloaded.

In runtime overloading the content of the functions are overriddenn in the child classes.


 #### Advantages over traditional programming model
 - OOP language allows breaking the program into bit-sized problems that can be solved easily (one object at a time).
 - OOP systems can be easily upgraded from small to large systems.
 - It is very easy to partition the work in a project based on objects
 - The principle of data hiding helps the programmer to build secure programs which cannot be invaded by the code in other parts of the program.
 - The data-centered design approach enables us to capture more details of model in an implementable form.







 **Resources**

 [A general Overview](https://en.wikipedia.org/wiki/Object-oriented_programming#:~:text=Object%2Doriented%20programming%20(OOP),(often%20known%20as%20methods).)
 [All topics related to OOPs in Java](https://www.geeksforgeeks.org/object-oriented-programming-oops-concept-in-java/?ref=lbp)
 [Advantages of oops](https://www.geeksforgeeks.org/benefits-advantages-of-oop/)
 [A detailed view on what an object means](https://www.techopedia.com/definition/24339/java-object#:~:text=A%20Java%20object%20is%20a,are%20also%20known%20as%20classes.)

