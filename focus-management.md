# Focus Management

**1. What is Deep Work?**
- Focusing on a task that currently we working on without any distractions of any kind for a specific period.
- Not switching the context from which we working on.
- Stick to a task for a long time without any distractions of any kind.

**2. Paraphrase all the ideas in the above videos and this one in detail.**

- Deep work is about performing professional activities with distraction-free concentration.
- Switching context is a bad thing and should be avoided in deep work.
- Recommended duration for deep work is 60 to 90 minutes.
- Deadlines are good for our productivity because it helps us to focus on the work by avoiding distractions.
- Setting deadlines to complete a task will push us to focus more on the work.
- At the same time, deadlines should not be taken as a scary thing.
- Intense periods of focus will cause myelin to develop around neurons which helps to fire our neurons quickly.
- Deep work allows us to rapidly connect our ideas and come up with a solution quickly.
- Deep work is difficult in the present fully distracted world.
- Avoiding distractions is hard and works only 50% of the time.
- Create a schedule for your deep work and distractions.
- Making deep work a regular habit can make us adapt easily.
- Planning the tasks for the next day will keep us relaxed at the end of the day.


**3. How can you implement the principles in your day to day life?**

- Make deep work a habit by practicing it every day.
- Schedule the deep work hours and distraction time.
- Set deadlines for a task to be finished.
- Focus on the work for at least 60 mins without any kind of distraction and do it at regular intervals.
- Take note of tasks for the next day.
- Practice deep work at a specific time in the day rather than a ad-hoc way.


**4. Your key takeaways from the video**

- Having social media is not that important to stay connected with people and friends.
- Social media is addictive and we think that it is giving us some entertainment on a busy day which is not true.
- By using social media we allow ourselves to be a source of income for companies by sharing our data.
- Avoiding social media can increase our productivity in professional work.
- Social media causes anxiety and increase stress levels which are harmful to our health.
- We can develop our skills better with no social media in our life by allowing us more time to work on skills that we need.
- A glance at social media can disturb our deep work.