## Grit and Growth

**1 Paraphrase (summarize) the video in a few lines. Use your own words**
The video speaks about how much grit is important for one's learning
- People who have grit in themselves can achieve more in their life than most talented people.
- The studies show that people with grit in themselves stand long even the challenges are tough.
- Following our commitments can help us grow grit.
- Grit is inversely proportional to talent.
- Having a growth mindset is the key to building grit.

**2 What are your key takeaways from the video to take action on?**

- Grit helps us to solve the toughest tasks.
- To build grit we should have a growth mindset.
- Committing to achieve something even though it's tough can make us build grit.
- Grit helps us to achieve greater things in our future selves. 


**3 Paraphrase (summarize) the video in a few lines in your own words**

The video talks about the following things
- Two mindsets of people toward learning, fixed and growth mindset.
- Fixed mindset people think skills and intelligence can't be improved and believe that they can't control themselves.
- Growth mindset people believe that skills and intelligence can be developed by working on them.
- People with a growth mindset tend to achieve more than a fixed mindset people over time.
- The most successful people believe that they can still improve themselves even after huge success in their life, stating that they are successful because they have a growth mindset.
- A growth mindset is a foundation for learning.
- Beliefs and focus are two keys that differentiate the growth and fixed mindset.
- Fixed mindset- Don't put effort to learn, don't take challenges, hate themselves for not having good skills and get discouraged finally they don't accept a feedback positively and take them personally. 
- Growth mindset- Believes putting effort benefits, taking challenges and pushing their limits, learning from mistakes, appreciating feedback from others.
- Accepting these key ingredients is based upon the belief and focus of a person.

**4 What are your key takeaways from the video to take action on?**

- Try to build a growth mindset.
- Believe that putting effort can improves us and give better results.
- Accept challenges and try to achieve them.
- Learn from mistakes don't get demotivated by them.
- Accept feedback from others and try to improve yourselves.

**5 What is the Internal Locus of Control? What is the key point in the video?**

- The outcomes of our work are in our control. Having control within ourselves is an internal locus of control.
- Having an internal locus of control is the key to motivation.
- People who believe putting effort can give better result are the one who stays motivated all the time.
- People who blame external factors are the one that can't keep motivated all the time.

**6 Paraphrase (summarize) the video in a few lines in your own words.**

The video speaks about how to develop a growth mindset.
- Believing that we can do or achieve something is the key to a growth mindset.
- Question who you are today and what you can become in future, don't think that you can't improve your capabilities.
- Don't believe that what you are today will be the same in the future, believe that you can improve yourselves
- Have your own curriculum for your life.
- Avoid discouragement when you fail and accept the challenges.

**7 What are your key takeaways from the video to take action on?**

- Believe in yourself that putting hard work can give you better results.
Questioning ourselves on what we want to become and what we can do for that can change our mindset.
- Focus on work can improve our results.
- Don't get discouraged when mistakes happened.
- Accept the challenges even though they are tough.

**8 What are one or more points that you want to take action on from the manual? (Maximum 3)**

- Stick to a problem till I complete it.
- I will understand each concept properly.
- I will have confidence in me and stand with it.


