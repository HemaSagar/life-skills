### Energy Management

**1 What are the activities you do that make you relax - Calm quadrant?**

- Meditate whenever got stressed.
- Sleeping or a short nap makes me get to a calm state.
- Looking at nature without doing anything.
- Listening to favorite music.

**2 When do you find getting into the Stress quadrant?**

- When the work is heavy and I can't do it.
- Whenever someone annoys me.
- In-adequate sleep.


**3 How do you understand if you are in the Excitement quadrant?**

- When I get something that I wanted for a long time.
- Doing thrilling tasks.
- I get the result that I expected for a task.


**4 Paraphrase the Sleep is your Superpower video in detail**

The video describes the following points
- Lack of sleep will decrease the days we live.
- It also effects the reproductive health of a person.
- Sleep before learning and sleep after learning can effect our memory differently.
- People who get enough sleep can have high learning rate than the people with less amount of sleep.
- People with no sleep can't hold the new memories.
- A person should get an uninterrupted deep sleep of 7 or more hours for good health.
- Even 1 hour less sleep can increase heart attack rate by 24%.
- Less sleep can lead to a decrease in the amount of Natural Killer Cells in our immune system by 70%.
- Gene modification is related to the amount of sleep we get.
- Sleeping in a cool temperature can make fall asleep quickly.


**5 What are some ideas that you can implement to sleep better?**

- Avoid staying awake for a long time at night.
- Have a daily sleep schedule for 8 hours a day.
- Sleep in a cooler atmosphere than a hotter one.
- Avoid doing night shifts at work.
- Don't use any electronic devices before half an hour for your sleep schedule.


**6 Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.**

The video talks about how exercising can effect our brain
- Exercising creates a good boost in our energy.
- It creates a better mood, attention and energy.
- Exercise increases the hormone secretion in our brain.
- It will increase out reaction times.
- Increase the focus of our brain. 


**7 What are some steps you can take to exercise more?**

- Do some exercises in the morning.
- Go for a walk in a clear environment.
- Make your body move at every part.
- Use staircase instead of lift.