## Learning Process

**1 What is the Feynman Technique? Paraphrase the video in your own words.**

Feynman Technique is a method of learning which shows our true potential and makes us try to understand the concept deeply and raise questions for ourselves. Whenever learning a new concept try to implement the following.

- Pick a topic to be learned.
- Understand the concepts and try to explain them to someone.
- Find out problematic and unclear topics, and try to understand them deeply.
- Simplify the concept in such a way a person with no base knowledge can also understand.

**2- What are the different ways to implement this technique in your learning process?**

- Select the topic to be learned.
- Go through the appropriate resources and collect the information.
- Refer to different resources for the same topic.
- After understanding the concepts deeply, explain them to another person.
- Practice the concepts by implementing them on your own.
- If stuck on any topic, refer to the resources again.


**3 Paraphrase the video in detail in your own words.**

- Adapt your brain to focus and love what you are learning.
- Shift your brain to focus mode while learning or solving a problem, it makes connecting the ideas better.
- Get relaxed when you are not able to solve in focus mode while relaxing your brain allows new thoughts to enter.
- Avoid procrastination, by taking small breaks and shifting focus again onto work.
- Don't do pleasant tasks, that will take your attention away.
- Adapt the Pomerado technique, focus on work for 25 mins and take a break for 5 mins and try to do entertaining things.
- Slow thinking is not a demerit, it allows us to grasp minute things, but need to work hard.

**4 What are some of the steps that you can take to improve your learning process?**

- Relax, when stuck on a problem for a long time, it will allow new thoughts to enter.
- Focus on the work, it makes thoughts connect easily.
- Avoid procrastination, by making ourselves try to complete the work and avoid doing pleasure-giving tasks.
- Adapting the Pomerado technique.


**5 Your key takeaways from the video? Paraphrase your understanding.**

- Get to know what are the skills that will help you by learning them. 
- Don't procrastinate by taking a big task to complete before putting it into practice.
- Put a small amount of knowledge you gained into practice.
- Don't allow distractions or barriers while learning.
- Practice these steps for the first 20 hours and you will get adapted to do them on your own later on.
- Overcome the frustration of you not be able to learn something new.




**6 What are some of the steps that you can while approaching a new topic?**

- Analyze what are the key skills that I need from the new topic.
- Learn in pieces and try to put that knowledge into practice.
- Don't try to learn everything and then practice it.
- Remove the distractions and try to focus on the learning process.
- Dive into learning without thinking too much.
- Invest the first 20 hours to implement these steps and it will become a habit.

