### Tiny Habits

**1 Your takeaways from the video**

- Tiny habits Behavior changes are systematic which means we can achieve them with systematic behavior.
- Break complex habits into tiny habits.
- Try to reduce stress when you are adapting a new habit.
- Appreciate yourself after performing a small habit.


###  Tiny Habits by BJ Fogg - Core Message


**2 Your takeaways from the video in as much detail as possible**

- To do hard work high motivation is needed and the lesser the toughness of the work lesser the motivation needed to take action.
- Shrink the habit as tiny as possible so that less motivation is needed to start the task.
- Breaking a complex habit into tiny habits will make it easier to achieve the complex habit.
- External and internal prompts are demotivating.
- Action prompts makes our current work motivate us to do the next task.
- Chain the tiny habits, so that one habit prompts us to do another one.
- Appreciating yourself after completing a habit will make you feel good and motivates you to do it often.
- Confidence and motivation increase with the frequency of success but not with the size of the success.
  


**3 How can you use B = MAP to make making new habits easier?**
B = MAP, behaviour can be changed by Motivation, Ability and Prompt.

-  Break the complex habits into tiny habits which will require less motivation to do it.
-  Appreciating ourselves after achieving a new habit increases motivation to do it often.
-  Action prompts can make us do the tasks easier.
  

**4 Why it is important to "Shine" or Celebrate after each successful completion of habit?**

- It will make us happy and motivates us to do it often.
- Making a small habit is easy, and also achieving it, so we will be appreciating ourselves often which boosts our confidence.
- The frequency of success gives more motivation and confidence than the size of success.

**5 Your takeaways from the video (Minimum 5 points)(1% better everyday)**

- 1% better every day makes us achieve more in the future years, whereas 1% worse everyday will take us below the level where we are today.
- Habits compound to our self-improvement.
- Notice what we need and make an implementation intention.
- Know what we want rather than having what we are presented with.
- Achieving a difficult goal requires more repetitions than for a less difficult task.
- So start doing the things that are harder to achieve.
- Good habits make time our allies and bad habits make it our enemy.
- Like the behaviors that you are practicing, because we do a task better if we like it.
- Continue doing what you have started don't break the chain.
- By doing actions you will become the person you wanted to be.


#### Book Summary of Atomic Habits

**6 Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?**

- Making a habit part of our identity will leads to ultimate form of motivation.
- Cue will trigger the brain to initiate an action
- Carving helps us to be motivated.
- The habit that we perform is the response from us.
- The end goal is the reward.
- Focus on the system rather than focusing on the outcomes.
- Outcomes are based on how systematically we work.


**7 Write about the book's perspective on how to make a good habit easier?**

- Make the habit more attractive.
- Make good habits easier by breaking them into tiny habits.
- Appreciate or celebrate yourself after achieving the habit.

**8 Write about the book's perspective on making a bad habit more difficult?**

- Make the habit hard to achieve.
- Feel yourself so disguised when you do it.
- Don't appreciate yourself after doing that habit.
- The process of doing that habit should be ugly.

#### Reflection


**9 Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?**

- Wake up early and sleep early.
- I will reward myself after waking up early in a day.
- I will treat my self with some good break fast.


**10 Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?**

Procrastinating is a habit that I want to eliminate.
- I will not appreciate myself if I procrastinate some task.
- Take away some rewards from my life after procrastinating.
- Make the early doing a habit.

