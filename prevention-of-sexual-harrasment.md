#### 1. What kinds of behavior cause sexual harassment?


- If anything hurts a person sexually whether it be verbal, visual or physical conduct and affects the person's health or work life that will lead to sexual harassment.

**Behaviors leading to sexual harassment**
- Talking about a person's physic, making jokes about clothing, requesting sexual favor, or spreading rumors about a person's sexual or personal life.
- Showing sexual posters, and cartoons, taking pictures of the person.
- Touching the person in an inappropriate way like kissing, hugging, rubbing, or staring offensively.
- Employers offering job rewards, and promotions to coarse employees into sexual relationships.
  
#### 2. What would you do in case you face or witness any incident or repeated incidents of such behavior?

- Inform the higher officials like supervisors, and managers about the incident if the harassment has happened between co-workers.
- If sexual harassment is caused by supervisors or higher officials workers then I will inform the government offices like the police.
Suggest organization make policies and take action on the one who harasses.
